import React from "react";
import { Container, Navbar, Nav, Button } from "react-bootstrap/";
import { useNavigate } from "react-router-dom";
import HomeIcon from '@mui/icons-material/Home';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import TvIcon from '@mui/icons-material/Tv';
import LocalMoviesIcon from '@mui/icons-material/LocalMovies';

const Header = () => {
  const navigate = useNavigate();
  return (
    <Navbar
      collapseOnSelect
      expand="md"
      variant="light"
      fixed="top"
      style={{ backgroundColor: "#323232", opacity: 0.5 }}
    >
      <Container className="justify-content-center">
        <Navbar.Brand>
          <h4 style={{ color: "white", cursor: "pointer" }}>Media4U</h4>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto" style={{ alignItems: "center" }}>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/")}
              data-toggle="tooltip"
              title="Home"
            >
              <HomeIcon/>
              Home
            </Nav.Link>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/")}
              data-toggle="tooltip"
              title="Trending"
            >
              <WhatshotIcon/>
              Trending
            </Nav.Link>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/")}
              data-toggle="tooltip"
              title="Movies"
            >
              <LocalMoviesIcon/>
              Movies
            </Nav.Link>
            <Nav.Link
              className="text-white"
              onClick={() => navigate("/")}
              data-toggle="tooltip"
              title="TV series"
            >
              <TvIcon/>
              Series
            </Nav.Link>
          </Nav>

          <Nav>
            <Button variant="light" style={{ borderRadius: "50px" }}>
              Login
            </Button>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
