import React, { useEffect, useState } from "react";
import { Carousel, Card, Row } from "react-bootstrap";

const Content = () => {
  const [filmToday, setFilmToday] = useState([]);
  const [filmOnAir, setFilmOnAir] = useState([]);
  const [tvOnAir, setTvOnAir] = useState([]);
  const [topRated, setTopRated] = useState([]);

  const getFilmToday = () => {
    fetch(
      "https://api.themoviedb.org/3/trending/all/day?api_key=7b6a815ec2221ce99a689fedf2357df1&page=1"
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setFilmToday(data.results.slice(0,7));
      })
      .catch((err) => console.log(err.message));
  };

  const getFilmOnAir = () => {
    fetch(
      "https://api.themoviedb.org/3/movie/now_playing?api_key=7b6a815ec2221ce99a689fedf2357df1&page=1"
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setFilmOnAir(data.results.slice(0,7));
      })
      .catch((err) => console.log(err.message));
  };

  const getTvOnAir = () => {
    fetch(
      "https://api.themoviedb.org/3/tv/on_the_air?api_key=7b6a815ec2221ce99a689fedf2357df1&page=1"
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setTvOnAir(data.results.slice(0,7));
      })
      .catch((err) => console.log(err.message));
  }

  const getTopRated = () => {
    fetch(
      "https://api.themoviedb.org/3/movie/top_rated?api_key=7b6a815ec2221ce99a689fedf2357df1&page=1"
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setTopRated(data.results.slice(0,7));
      })
      .catch((err) => console.log(err.message));
  }

  useEffect(() => {
    getFilmToday();
    getFilmOnAir();
    getTvOnAir();
    getTopRated();
  }, []);

 

  return (
    <div>
      <div style={{ marginTop: "-55px" }}>
        <Carousel fade>
          {filmToday.map((film, index) => {
            return (
              <Carousel.Item key={index}>
                <img
                  className="d-block w-100"
                  src={`https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces//${film.backdrop_path}`}
                  alt="film poster"
                />
                <Carousel.Caption
                  style={{
                    textAlign: "left",
                    marginLeft: "-39px",
                    fontWeight: "bolder",
                  }}
                >
                  <h3>{film.title || film.name}</h3>
                  <p>{film.media_type}</p>
                  <p>{film.overview}</p>
                </Carousel.Caption>
              </Carousel.Item>
            );
          })}
        </Carousel>
      </div>
      <div className="mt-3">
        <h3>On air:</h3>
        <Row>
          {filmOnAir.map((film, index) => {
            return (
              <Card
                className="shadow rounded col-1 mx-1 my-1"
                style={{ minHeight: "60px", maxHeight: "150px" }}
                key={index}
              >
                <Card.Img
                  src={`https://image.tmdb.org/t/p/w300//${film.poster_path}`}
                  alt={film.name}
                  style={{ cursor: "pointer", minWidth: "50px", minHeight: "30px" }}
                />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      alignItems: "center",
                      fontSize: "15px",
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                      color: "burlywood"
                    }}
                  >
                    {film.name || film.title}
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            );
          })}
        </Row>
      </div>
      <div className="mt-3">
        <h3>TV on air:</h3>
        <Row>
          {tvOnAir.map((film, index) => {
            return (
              <Card
                className="shadow rounded col-1 mx-1 my-1"
                style={{ minHeight: "60px", maxHeight: "150px" }}
                key={index}
              >
                <Card.Img
                  src={`https://image.tmdb.org/t/p/w300//${film.poster_path}`}
                  alt={film.name}
                  style={{ cursor: "pointer", minWidth: "50px", minHeight: "30px" }}
                />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      alignItems: "center",
                      fontSize: "15px",
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                      color: "burlywood"
                    }}
                  >
                    {film.name || film.title}
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            );
          })}
        </Row>
      </div>
      <div className="mt-3">
        <h3>Top rated:</h3>
        <Row>
          {topRated.map((film, index) => {
            return (
              <Card
                className="shadow rounded col-1 mx-1 my-1"
                style={{ minHeight: "60px", maxHeight: "150px" }}
                key={index}
              >
                <Card.Img
                  src={`https://image.tmdb.org/t/p/w300//${film.poster_path}`}
                  alt={film.name}
                  style={{ cursor: "pointer", minWidth: "50px", minHeight: "30px" }}
                />
                <Card.ImgOverlay>
                  <Card.Title
                    style={{
                      alignItems: "center",
                      fontSize: "15px",
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                      color: "burlywood"
                    }}
                  >
                    {film.name || film.title}
                  </Card.Title>
                </Card.ImgOverlay>
              </Card>
            );
          })}
        </Row>
      </div>
    </div>
  );
};

export default Content;
